
<?php
// PDO DB connection
 try {
     //Data Source Name
     $dsn = "mysql:host=localhost;dbname=airtime_dispenser";
     $db = new PDO($dsn, 'root', '');
     $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     //Testing DB Connection
     //die(json_encode(array('The DB Is Connected!' => true)));
 } catch (PDOException $e) {
     echo '<p class="bg-warning">'.$e->getMessage(); '</p>';
 }
