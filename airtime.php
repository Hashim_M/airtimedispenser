<?php
require_once "includes/AfricasTalkingGateway.php";

function uploadFile(){
    if ( isset($_FILES["csv_file"])) {

        //if there was an error uploading the file
        if ($_FILES["csv_file"]["error"] > 0) {
            echo '<div class="alert-warning"><p>'."No File Uploaded: " . $_FILES["csv_file"]["error"] .'</p></div>';

        }
        else {
            //if file already exists
            if (file_exists("upload/" . $_FILES["csv_file"]["name"])) {
                echo '<div class="alert-success"><p>'. $_FILES["csv_file"]["name"]."  Already Exist!".'</p></div>';
                //Saving CSV to database
                $file = fopen( "upload/".$_FILES["csv_file"]["name"], "r");
                try {
                 include_once "includes/config.php";
                 while ($data = fgetcsv($file,1000,",","'")) {
                        $dsn = "mysql:host=localhost;dbname=airtime_dispenser";
                        $db = new PDO($dsn, 'root', '');
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "INSERT INTO employees ( employee_name, PhoneNumber, Amount, status) VALUES (?, ?, ?, ?)";
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(1, $data[0], PDO::PARAM_STR);
                        $number = (float)$data[1];
                        $stmt->bindParam(2, $number, PDO::PARAM_STR);
                        $stmt->bindParam(3, $data[2], PDO::PARAM_STR);
                        $status = "Pending";
                        $stmt->bindParam(4, $status, PDO::PARAM_STR);
                        $stmt->execute();
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                }
                } catch (PDOException $e) {
                    echo '<div class="alert-success"><p>'.$e->getMessage().'</p></div>';
                    }
                echo '<div class="alert-success"><p>'."File Uploaded To DB!".'</p></div>';

                }
                else {

                $stored_name  = $_FILES["csv_file"]["name"];
                move_uploaded_file($_FILES["csv_file"]["tmp_name"], "upload/" . $stored_name);
                echo '<div class="alert-success"><p>'. $_FILES["csv_file"]["name"]."  Uploaded!".'</p></div>';

                //converting CSV to Json
                $file = fopen( "upload/" .$stored_name, "r");
                try {
                    while ($data = fgetcsv($file,1000,",","'")) {
                        $dsn = "mysql:host=localhost;dbname=airtime_dispenser";
                        $db = new PDO($dsn, 'root', '');
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "INSERT INTO employees ( employee_name, PhoneNumber, Amount, status) VALUES (?, ?, ?, ?)";
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(1, $data[0], PDO::PARAM_STR);
                        $number = (float)$data[1];
                        $stmt->bindParam(2, $number, PDO::PARAM_STR);
                        $stmt->bindParam(3, $data[2], PDO::PARAM_STR);
                        $status = "Pending";
                        $stmt->bindParam(4, $status, PDO::PARAM_STR);
                        $stmt->execute();
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    }
                } catch (PDOException $e) {
                    echo '<div class="alert-success"><p>'.$e->getMessage().'</p></div>';
                }
            }
        }
    } else {
        echo  '<div class="alert-warning"><p>'."No File Uploaded".'</p></div>';
    }
}

function getData(){
    $dsn = "mysql:host=localhost;dbname=airtime_dispenser";
    $db = new PDO($dsn, 'root', '');
    $query = "SELECT * FROM employees";
    $results = $db->query($query);
    $rows = $results->fetchAll();


    return $rows;
}

function getRecipients (){
    $dsn = "mysql:host=localhost;dbname=airtime_dispenser";
    $db = new PDO($dsn, 'root', '');
    $query = "SELECT PhoneNumber, Amount FROM employees";
    $results = $db->query($query);
    $rows = $results->fetchAll();
    return $rows;
}

function sendAirtime(){
    $the_rows = getRecipients();
    $username = "sandbox";
    $apiKey   = "6ef89b9f09c146c5cd936f20aeeac0d21486c48bb8cc6c4fd2afbe51b10f52d9";

    $recipients = $the_rows;

    $recipientStringFormat = json_encode($recipients);
    $gateway = new AfricasTalkingGateway($username, $apiKey);
    try {
        $results = $gateway->sendAirtime($recipientStringFormat);
        foreach($results as $result) {
            echo $result->status;
            echo $result->amount;
            echo $result->phoneNumber;
            echo $result->discount;
            echo $result->requestId;
            echo $result->errorMessage;
        }
    }
    catch(AfricasTalkingGatewayException $e){
        echo '<div class="alert-success"><p>'.$e->getMessage().'</p></div>';
    }


}