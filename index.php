<?php
include 'includes/config.php';
include "airtime.php";

if (isset($_POST['send'])){
    sendAirtime();
    header('location: index.php');
    echo '<div class="alert-success"><p>'.'Airtime Sent Awaiting Confirmation!'.'</p></div>';
}

if (isset($_POST['upload'])){
    uploadFile();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Airtime | Dispenser</title>
    <link rel="stylesheet" type="text/css" href="dist/css/style.css">
    <!--[if lt IE 9]>
    <script scr="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script scr="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="header">
            <h2>XYZ Limited</h2>
            <h4>Airtime Dispenser</h4>
        </div>
    </div>
    <div class="row">
        <div class="upload">
            <h5>Upload CSV file</h5>
            <form action="index.php" method="post" enctype="multipart/form-data">
                <input type="file" name="csv_file">
                <button type="submit" name="upload">Upload</button>
            </form>
        </div>
        <form method="post" action="index.php">
            <div class="send-button">
                <button type="submit" name="send">Send Airtime</button>
            </div>
        </form>
    </div>
    <div class="row table">
        <table class="table table-bordered table-responsive table-stripped">
            <thead>
            <tr>
                <th>Employee Name</th>
                <th>Phone Number</th>
                <th>Amount</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <?php
            $rows = getData();
            foreach ($rows as $row){?>
            <tr>
                <td><?=$row['employee_name']; ?></td>
                <td><?=$row['PhoneNumber']; ?></td>
                <td><?=$row['Amount']; ?></td>
                <td><?=$row['status']; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>